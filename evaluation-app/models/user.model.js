var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var UserSchema = Schema({
  nick: {
    type: String,
    required: true,
    unique: true,
    match: /(^_|[a-zA-Z])[a-zA-Z]+$/,
  },
  nombre: { type: String, required: true },
  apellidos: String,
  password: { type: String, required: true },
  rol: {
    type: String,
    enum: ["Admin", "Operador", "Administrativo"],
    required: true,
  },
  email: {
    type: String,
    required: true,
    match: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
  },
});

module.exports = mongoose.model("User", UserSchema);
