var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var mongoose = require("mongoose");

//Views Routes
var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");

// API Routes (CRUD)
var usersAPIRouter = require("./routes/users.routes");

var config = require("./config.js");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

// MongoDB
mongoose.connect(
  "mongodb://localhost:27017/" + config.database,
  {
    useNewUrlParser: true,
  },
  (err) => {
    if (err) throw err;
    console.log("MongoDB: \x1b[32m%s\x1b[0m", "Successfully running MongoDB");
  }
);

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
/**
 * Views render
 */
app.use("/", indexRouter);
app.use("/users", usersRouter);

/**
 * Api routes
 */

app.use("/api", usersAPIRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
