function addUser() {
  document.location.pathname = "/users/registro";
}

function updateUser(id) {
  document.location.pathname = "/users/registro/" + id;
}

function deleteUser(id) {
  var url = "../../api/users/";
  var xhr = new XMLHttpRequest();
  xhr.open("DELETE", url + id, true);
  xhr.onload = function () {
    document.location.pathname = "/users";
  };
  xhr.send(null);
}

function onSubmitUser(verb) {
  console.log(verb);
  var nick = document.getElementById("nick").value,
    name = document.getElementById("name").value,
    apellidos = document.getElementById("apellidos").value,
    pw = document.getElementById("pw").value,
    role = document.getElementById("role").value,
    email = document.getElementById("email").value;
  var user = {
    nick: nick,
    nombre: name,
    apellidos: apellidos,
    password: pw,
    rol: role,
    email: email,
  };
  sendUser();

  function sendUser() {
    var url = "../../api/users/";
    if (verb === "PUT") url += `${window.location.href.split("/").pop()}`;
    var json = JSON.stringify(user);

    var xhr = new XMLHttpRequest();
    xhr.open(verb, url, true);
    xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
    xhr.onload = function () {
      document.location.pathname = "/users";
    };
    xhr.send(json);
    console.log("USER", user);
  }
}

function nextPage() {
  console.log(min, max, page, pages);
}
