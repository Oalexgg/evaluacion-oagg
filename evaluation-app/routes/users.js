var express = require("express");
var router = express.Router();
var UsuarioModel = require("../models/user.model");

router.get("/", async function (req, res, next) {
  var usuarios = await UsuarioModel.find();
  res.render("users", {
    usuarios: usuarios,
  });
});

router.get("/registro/:id?", async function (req, res, next) {
  var responseObject = {
    title: "Registro",
    verb: "POST",
  };
  var id = req.params.id;
  if (id) {
    var user = await UsuarioModel.findById(id);
    console.log(user);
    if (typeof user != undefined && typeof user != null) {
      responseObject.user = user;
    }
    responseObject.title = "Actualizar";
    responseObject.verb = "PUT";
  }
  console.log(responseObject);
  res.render("registro", responseObject);
});

module.exports = router;
