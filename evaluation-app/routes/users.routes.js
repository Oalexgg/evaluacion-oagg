var express = require("express");
var router = express.Router();
var UsuarioModel = require("../models/user.model");

router.get("/users/:id?", function (req, res, next) {
  if (req.params.id) {
    console.log(req.params.id);
  } else {
    console.log(" no mandó param");
  }
  res.status(200).send();
});

router.post("/users", async function (req, res, next) {
  var body = req.body;
  var usuario = new UsuarioModel({
    nick: body.nick,
    nombre: body.nombre,
    apellidos: body.apellidos,
    password: body.password,
    rol: body.rol,
    email: body.email,
  });
  try {
    await usuario.save();
    console.log("Anda por acá");
    res.redirect(200, "../users");
  } catch (e) {
    console.log(e);
    res.redirect("../users/registro", { error: e });
  }
});

router.put("/users/:id", async function (req, res, next) {
  var body = req.body;
  var id = req.params.id;
  try {
    await UsuarioModel.findByIdAndUpdate(id, {
      nick: body.nick,
      nombre: body.nombre,
      apellidos: body.apellidos,
      password: body.password,
      rol: body.rol,
      email: body.email,
    });
    res.redirect(200, "../users");
  } catch (e) {
    // console.log(e);
    res.redirect("../users/registro", { error: e });
  }
});

router.delete("/users/:id", async function (req, res, next) {
  var id = req.params.id;
  try {
    await UsuarioModel.findByIdAndDelete(id);
    res.redirect(200, "../users");
  } catch (e) {
    res.status(500).send({
      error: e,
    });
  }
});

module.exports = router;
